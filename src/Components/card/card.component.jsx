import "./card-list.style.scss";
export const Card = ({ user, img}) => {
    return (
        <div className="child-container">
            <img src={img} alt={user.name} />
            <div className="card-user">
                <h3>{ user.name }</h3>
                <p>{ user.email }</p>
            </div>            
        </div>
    );
};