import { Card } from '../card/card.component';
import './card-list.style.scss';

export const CardList = ({users}) => {
    
    return (
    <div className="card-container">
        {
            users.map((user)=>(
                <Card key={user.id} user={user}  img={`https://robohash.org/${user.id}?set=set2&size=200x200`}/>
            ))
        }        
    </div>
)};
