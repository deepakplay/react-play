import React from 'react';
import "./search.style.css";

export const SearchBox = ({ placeholder = '', handle = () => { } }) => {
    return (
        <input type="text" placeholder={placeholder} className="search_input" onChange={handle} />
    );
}