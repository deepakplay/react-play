import { Component } from "react";
import { CardList } from "../Components/card-list/card-list.component";
import { SearchBox } from "../Components/serach-box/search.component";

import "./main.style.scss";

export default class Main extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            searchKey: undefined
        }
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => response.json())
            .then((data) => this.setState({ users: data }));
    }

    onSearch = (e) => {
        this.setState({ searchKey: e.target.value });
    }

    render() {
        let users = this.state.users;

        if (this.state.searchKey) {
            users = users.filter((user) => {
                return user.name.toLowerCase().includes(this.state.searchKey.toLowerCase().trim());
            });
        }

        return (
            <div className="container">                
                <SearchBox placeholder="Search..." handle={this.onSearch} />
                <CardList users={users} />
            </div>
        );
    }
}